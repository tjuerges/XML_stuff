<?xml version="1.0" encoding="iso-8859-1"?>
<!--
    Transform an OPML playlist into an M3U playlist.
    $Id$

    who       when      what
    ========  ========  ==============================================
    tjuerges  2007-03-19  created
    -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"></xsl:output>
    <xsl:template match="body">
        <xsl:value-of select="text()"></xsl:value-of>
        <xsl:text>#EXT3MU&#x0A;</xsl:text>
        <xsl:for-each select="outline">
            <xsl:sort select="@URL"></xsl:sort>
            <xsl:sort select="@text"></xsl:sort>
            <xsl:text>#EXTINF:</xsl:text>
            <xsl:value-of select="@text"></xsl:value-of>
            <xsl:value-of select="name()"></xsl:value-of>
            <xsl:value-of select="text()"></xsl:value-of>
            <xsl:text>&#x0A;</xsl:text>
            <xsl:value-of select="@URL"></xsl:value-of>
            <xsl:value-of select="text()"></xsl:value-of>
            <xsl:text>&#x0A;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
