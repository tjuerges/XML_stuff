<?xml version="1.0" encoding="iso-8859-1"?>
<!--
    ALMA - Atacama Large Millimiter Array
    (c) Associated Universities Inc., 2007

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

    $Id$

    who       when      what
    ========  ========  ==============================================
    tjuerges  2007-03-19  created
    -->
<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output
        method="text"></xsl:output>
    <xsl:template
        match="/">
        <xsl:for-each
            select="Log/*[name() != 'Header']">
            <xsl:sort
                select="@TimeStamp"></xsl:sort>
            <xsl:sort
                select="@LogId"></xsl:sort>
            <xsl:value-of
                select="@TimeStamp"></xsl:value-of>
            <xsl:text> [</xsl:text>
            <xsl:value-of
                select="upper-case(name())"></xsl:value-of>
            <xsl:text>]</xsl:text>
            <xsl:if
                test="@Audience != ''">
                <xsl:text> [Audience=</xsl:text>
                <xsl:value-of
                    select="@Audience"></xsl:value-of>
                <xsl:text>]</xsl:text>
            </xsl:if>
            <xsl:if
                test="@Host != ''">
                <xsl:text> (</xsl:text>
                <xsl:value-of
                    select="@Host"></xsl:value-of>
                <xsl:text>) </xsl:text>
            </xsl:if>
            <xsl:if
                test="@LogId != ''">
                <xsl:text>LogId=</xsl:text>
                <xsl:value-of
                    select="@LogId"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <!-- Uncomment this section to display the URI field.
                <xsl:if test="@URI != ''">
                <xsl:text>URI=</xsl:text>
                <xsl:value-of select="@URI"></xsl:value-of>
                <xsl:text>, </xsl:text>
                </xsl:if>
            -->
            <xsl:if
                test="(@Process != '') and ((@Process != 'LoggerName') and (@Process != 'maciContainer'))">
                <xsl:text>Process=</xsl:text>
                <xsl:value-of
                    select="@Process"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if
                test="@SourceObject != ''">
                <xsl:text>SourceObject=</xsl:text>
                <xsl:value-of
                    select="@SourceObject"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <!-- Uncomment this section to display the Context section.
                <xsl:if test="@Context != ''">
                <xsl:text>Context=</xsl:text>
                <xsl:value-of select="@Context"></xsl:value-of>
                <xsl:text>, </xsl:text>
                </xsl:if>
            -->
            <xsl:if
                test="(@Thread != '') and ((@Thread != 'ORBTask') and (@Thread != 'loggerThread'))">
                <xsl:text>Thread=</xsl:text>
                <xsl:value-of
                    select="@Thread"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if
                test="@StackId != ''">
                <xsl:text>StackId=</xsl:text>
                <xsl:value-of
                    select="@StackId"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if
                test="(@Routine != '') and (@Routine != 'loggerThread')">
                <xsl:text>Routine=</xsl:text>
                <xsl:value-of
                    select="@Routine"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if
                test="@File != ''">
                <xsl:text>File=</xsl:text>
                <xsl:value-of
                    select="@File"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if
                test="@Line != ''">
                <xsl:text>Line=</xsl:text>
                <xsl:value-of
                    select="@Line"></xsl:value-of>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:text>- </xsl:text>
            <xsl:if
                test="@Data != ''">
                <xsl:text>Data=</xsl:text>
                <xsl:value-of
                    disable-output-escaping="yes"
                    select="@Data"></xsl:value-of>
                <xsl:text>FOOOOOO </xsl:text>
            </xsl:if>
            <xsl:value-of
                select="text()"></xsl:value-of>
            <xsl:text>&#x0A;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
